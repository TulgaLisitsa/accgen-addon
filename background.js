var HEADERS_TO_STRIP_LOWERCASE = [
	'content-security-policy',
	'x-frame-options',
];


function setHeader(e) {
	if (!e.responseHeaders || (!e.originUrl && !e.initiator))
		return;
	for (var header of e.responseHeaders) {
		if (header.name.toLowerCase() === "x-frame-options") {
			header.value = "ALLOW";
		} else if (header.name.toLowerCase() === "content-security-policy") {
			header.value = header.value.replace(/frame-ancestors[^;]*;?/, "frame-ancestors http://* https://*;")
		}
	}
	var myHeader = {
		name: "x-frame-options",
		value: "ALLOW"
	};
	e.responseHeaders.push(myHeader);

	var url;
	if (typeof browser != "undefined")
		url = new URL(e.originUrl);
	else
		url = new URL(e.initiator)

	var myHeader2 = {
		name: "Access-Control-Allow-Origin",
		value: url.host.replace(/^[^.]+\./g, "") == "cathook.club" ? url.origin : "https://accgen.cathook.club"
	};
	e.responseHeaders.push(myHeader2);

	return {
		responseHeaders: e.responseHeaders.filter(function (header) {
			return HEADERS_TO_STRIP_LOWERCASE.indexOf(header.name.toLowerCase()) < 0;
		})
	};
}

if (typeof browser != "undefined") {
	//firefox
	browser.webRequest.onHeadersReceived.addListener(setHeader, {
		urls: ["https://*.cathook.club/*", "https://store.steampowered.com/*"]
	}, ["blocking", "responseHeaders"]);
} else {
	//chrome
	chrome.webRequest.onHeadersReceived.addListener(setHeader, {
		urls: ["https://*.cathook.club/*", "https://store.steampowered.com/*"]
	}, ["blocking", "responseHeaders", "extraHeaders"]);
}
